# SEO

Alphaweb SEO is a small JavaScript for SEO optimizing your website.
If you has a webpage such as https://www.alphaweb.dk/soegeordsanalyse-en-vigtig-del-af-din-online-strategi/ you would of course want to SEO optimize it, and with this free tool it will be a lot easier.
For most companies SEO (together with social media and Google ads) are an important part of their online marketing strategi. But SEO can be pretty hard to do right. This is why we have decided to start on this JavaScript tool which will automatically do the basic search engine optimizations on your page. You just add your main keyword and the script will place it in it in various tags across your website, bold it and other seo techniques to improve your online visibility.